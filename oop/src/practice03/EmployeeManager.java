package practice03;

public class EmployeeManager {
	
	public static void main(String[] args) {
	
	Employee employee1 = new Employee("James Wright", 42, "Manager", 20000);
	Employee employee2 = new Employee("Amy Smith", 27, "Design Coordinator", 8000 , 15);
	Employee employee3 = new Employee("Peter Coolidge", 32, "Assistant Manager", 12000, 7);
	Employee employee4 = new Employee("John Doe", 22, "Engineer", 10000, 10); 
	

	
	System.out.println(employee1.toString());
	System.out.println(employee2.toString());
	System.out.println(employee3.toString());
	System.out.println(employee4.toString());
	
	Employee employee5 = new Employee("Yoomin Lee", 20);
	System.out.println(employee1.equals(employee5));
	System.out.println(employee2.equals(employee5));
	System.out.println(employee3.equals(employee5));
	System.out.println(employee4.equals(employee5));
	
	employee1.vacation(10);
	employee3.vacation(10);
	
	System.out.println(employee1.toString());
	System.out.println(employee2.toString());
	System.out.println(employee3.toString());
	System.out.println(employee4.toString());
	System.out.println(employee5.toString());	
	}

	
}