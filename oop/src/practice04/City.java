package practice04;
import java.util.Random;

import practice03.Employee;

public class City {
	
	private String name;
	private int location_x;
	private int location_y;
	
	Random rnd = new Random();
	
	public City(String n, int locx, int locy) {
		this.name = n;
		this.location_x = locx;
		this.location_y = locy;
	}
	
	public City(String n) {
		this.name = n;
		this.location_x = rnd.nextInt(359);
		this.location_y = rnd.nextInt(359);
	}
	
	public String getName() {
		return name;
	}
	 public void setName(String n) {
    	this.name = n;
    }
    public int getLocx() {
    	return location_x;
    }
    public void setLocx(int locx) {
    	this.location_x = locx;
    }
    public int getLocy() {
    	return location_y;
    }
    public void setLocy(int locy) {
    	this.location_y = locy;
    }	
    
    public boolean equals(City anotherCity) {
    	if(this.name.equals(anotherCity.name) && this.location_x == anotherCity.location_x && this.location_y == anotherCity.location_y)
		{System.out.println("같은도시 입니다");
		return true;}
	else
		{System.out.println("같은도시가 아닙니다");
		return false;}
}


    public String toString() {
	
    	return  name + ", "+ location_x +", "+ location_y;
    	}
    
    public String distance(City name1, City name2) {
    	double x2 = Math.pow(name1.location_x - name2.location_x, 2);
    	double y2 = Math.pow(name1.location_y - name2.location_y, 2);
    	double dis = Math.sqrt(x2+y2);
    	return name1.name + "-" + name2.name + " : " + dis;
    }
    

}
