package practice04;

public class Lab04 {

	public static void main(String[] args) {
		
		
		City c1 = new City("Seoul", 23,45);
		City c2 = new City("Paris", 123,41);
		City c3 = new City("Racoon City");
		City c4 = new City("Mega City");


		System.out.println(c1.toString());
		System.out.println(c2.toString());
		System.out.println(c3.toString());
		System.out.println(c4.toString());
		
		System.out.println(c1.distance(c1, c2));
		System.out.println(c1.distance(c1, c3));
		System.out.println(c1.distance(c2, c4));
		
	}

}
