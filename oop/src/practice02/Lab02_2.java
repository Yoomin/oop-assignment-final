package practice02;



import java.util.Scanner;

public class Lab02_2 {
	
	private static String makeOrdinalNumber(int num) {
		// 11, 12, 13, 111, 112, 113.. 인 경우 무조건 숫자 뒤에 th가 붙으므로 전처리
		if((num % 100) / 10 == 1)
		return num + "th";

		switch(num % 10) {
		case 1 : 
		return num + "st";

		case 2 : 
		return num + "nd";

		case 3 : 
		return num + "rd";
		}

		return num + "th";
		}
	public static void main(String[] args) {
			
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		String inputDw = input.toLowerCase();
		
		String[]sc = inputDw.split(" ");
		
		int all = 0;

		
		for(int i = 0; i < sc.length ; i++) {
			String s = sc[i];
			
			int score = 0;
			
			if(s.contentEquals("a")) {
				score = 100;
			}else if(s.contentEquals("b")) {
				score = 90;
			}else if(s.contentEquals("c")) {
				score = 80;
			}else if(s.contentEquals("d")) {
				score = 70;
			}else {
				score = 0;
			}
			
			System.out.println(makeOrdinalNumber(i+1) + " student's score is "+ score);
			

			all = all + score;	
		}
		
				
		float average = all / sc.length;
		
		System.out.printf("This class's average = %.2f ", average);
		
	}
		
}
	
