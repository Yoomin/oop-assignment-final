package kr.co.lab05.employee;

public class Employee {
	
	private String name;
	private double yearly_salary;
	private double monthly_salary;
	private double balance;

	public Employee(String nName, double nYearly_salary) {
		name = nName;
		yearly_salary = nYearly_salary;
		monthly_salary = nYearly_salary/12;
		balance = 0;
		
	}
	//balance 반환하는 getter
	public double getBalance(Employee emp) {
		return emp.balance;
	}

	public double getYearly_salary(Employee emp) {
		return emp.yearly_salary;
	}
	
	public void setYearly_salary(Employee emp, double y) {
		emp.yearly_salary = y;
	}
	//연봉 증가
	public void increaseYearlySalary(Employee emp, int byPercent) {
		emp.yearly_salary = emp.yearly_salary + emp.yearly_salary * (byPercent/100);
		emp.monthly_salary = emp.yearly_salary/12;
	}
	//월급 받는 method
	public void receiveSalary(Employee emp) {
		emp.balance = emp.balance + emp.monthly_salary;
	}
	//toString 메서드
	public String toString(Employee emp) {
		return "이름 : " + emp.name + " 연봉 : "  + emp.yearly_salary + " 월급 : " + emp.monthly_salary + " 재산 : " + emp.balance;
	}
	
}
