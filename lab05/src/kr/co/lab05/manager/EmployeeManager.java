package kr.co.lab05.manager;

import java.time.LocalDate;
import java.util.Random;

public class EmployeeManager {
	
	static Random rnd = new Random();
	 
	public static void main(String[] args) {

		LocalDate myDate = LocalDate.of(2020, 4, 16);

		kr.co.lab05.employee.Employee e = new kr.co.lab05.employee.Employee("Lee Yoo Min", 4500);
		
		//계약일 출력
		System.out.println("계약일: " + myDate.toString());
		//직원 정보 출력
		System.out.println(e.toString(e));
		
		//인센티브 받는 랜덤 달
		int insMonth = rnd.nextInt(12)+1;

		double balance = e.getBalance(e);
			
		while(balance<20000) {
			e.receiveSalary(e);
			//결과 이상하게 나왔던 이유 :
			//밑의 줄에서 'myDate = ' 이거 안써
			myDate = myDate.plusMonths(1);
			balance = e.getBalance(e);

			if(myDate.getMonthValue() == insMonth) {
				e.receiveSalary(e);
				int pYear = myDate.getYear();
				System.out.println((pYear - 2019) + "년차 " + insMonth + "월에 인센티브를 받았습니다."); 
				balance = e.getBalance(e);
			}
			//근무한지 12개월 지났을 때 연봉 증가
			if(myDate.getMonthValue() == 4 && myDate.getYear() != 2020) {
				int incYsalary = rnd.nextInt(11);
				//
				e.increaseYearlySalary(e, incYsalary);
				int pYear = myDate.getYear();
				System.out.println((pYear - 2019) + "년차 연봉이 " + incYsalary + "% 인상되습니다.");
				insMonth = rnd.nextInt(12)+1;
			}
			
		}

		System.out.println("날짜: " + myDate.toString());
		System.out.println(e.toString(e));
		
		
	}
}
		
		





